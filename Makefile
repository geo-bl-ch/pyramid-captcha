SRC = $(shell find pyramid_captcha -name '*.py')

ifeq (, $(shell which virtualenv))
	VENV ?= python3 -m venv
else
	VENV ?= virtualenv
endif

.venv/venv.timestamp:
	$(VENV) .venv
	touch $@

.venv/requirements.timestamp: .venv/venv.timestamp setup.py requirements.txt
	.venv/bin/pip install -r requirements.txt
	touch $@

.venv/doc-requirements.timestamp: .venv/requirements.timestamp doc-requirements.txt
	.venv/bin/pip install -r doc-requirements.txt
	touch $@

build/build.timestamp: .venv/requirements.timestamp setup.py setup.cfg MANIFEST.in $(SRC)
	.venv/bin/python setup.py clean check sdist bdist_wheel
	touch $@

.PHONY: clean
clean:
	rm -rf .venv
	rm -rf .sessions
	rm -rf pyramid_captcha.egg-info
	rm -rf build
	rm -rf dist

.PHONY: git-attributes
git-attributes:
	git --no-pager diff --check `git log --oneline | tail -1 | cut --fields=1 --delimiter=' '`

.PHONY: lint
lint: .venv/requirements.timestamp setup.cfg $(SRC)
	.venv/bin/python --version
	.venv/bin/flake8

.PHONY: test
test: .venv/requirements.timestamp setup.cfg $(SRC)
	.venv/bin/python --version
	.venv/bin/py.test

.PHONY: build
build: build/build.timestamp

.PHONY: deploy
deploy: build
	.venv/bin/twine upload dist/*

.PHONY: serve
serve: .venv/requirements.timestamp
	.venv/bin/python demo/demo.py

.PHONY: doc
doc: .venv/doc-requirements.timestamp
	.venv/bin/sphinx-versioning build doc/source doc/build/html
