.. Pyramid Captcha documentation master file, created by
   sphinx-quickstart on Tue Nov  5 12:18:17 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Pyramid Captcha
===============

.. toctree::
   :maxdepth: 2
   :hidden:

   pyramid_captcha
   pyramid_captcha_exception
   usage
   changelog

This Python package provides a captcha implementation for use with the
`Pyramid Web Framework <https://docs.pylonsproject.org/projects/pyramid/en/latest/>`__.
It is based on the `captcha <https://pypi.org/project/captcha/>`__ library and uses
Pyramid sessions for captcha validation. The recommended plugin for secure session
handling is `pyramid_beaker <https://pyramid-beaker.readthedocs.io/en/latest/>`__.

You can install the package from `PyPI.org <https://pypi.org/project/pyramid-captcha/>`__
using :code:`pip` or download the source code on
`GitLab <https://gitlab.com/geo-bl-ch/pyramid-captcha>`__.

.. note:: The latest development builds are available on https://test.pypi.org/project/pyramid-captcha/.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
