Module *pyramid_captcha*
========================

.. automodule:: pyramid_captcha


Captcha
-------

.. autoclass:: Captcha
    :members:
    :show-inheritance:
