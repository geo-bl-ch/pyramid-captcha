Module *pyramid_captcha.exception*
==================================

.. automodule:: pyramid_captcha.exception


CaptchaError
------------

.. autoclass:: CaptchaError
   :members:
   :show-inheritance:


CaptchaMissingError
-------------------

.. autoclass:: CaptchaMissingError
   :members:
   :show-inheritance:


CaptchaMismatchError
--------------------

.. autoclass:: CaptchaMismatchError
   :members:
   :show-inheritance:
