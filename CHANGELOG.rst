Changelog
=========

1.0.0
-----

https://gitlab.com/geo-bl-ch/pyramid-captcha/-/milestones/1

- Add parameter for value validation
- Configurable captcha length
- Initial version
