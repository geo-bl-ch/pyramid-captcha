# -*- coding: utf-8 -*-
import os

from mako.template import Template
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid_beaker import session_factory_from_settings
from waitress import serve

from pyramid_captcha import Captcha
from pyramid_captcha.exception import CaptchaError

template_index = """
<html>

<head>
    <title>Captcha Test</title>
</head>

<body>
    <h1>Captcha Test</h1>
    <p><img src="captcha/generate"></p>
    <p>
        <form method="POST" action="validate">
            <input type="text" maxlength="6" size="6" name="captcha" />
            <input type="submit" value="Validate!" />
        </form>
    </p>
</body>

</html>
"""

template_validate = """
<html>

<head>
    <title>Captcha Test</title>
</head>

<body>
    <h1>Captcha Test</h1>
    <p>${result}</p>
    <p><a href="/">back</a></p>
</body>

</html>
"""


def index(request):
    response = Response(template_index)
    response.status_int = 200
    response.content_type = 'text/html'
    return response


def validate(request):

    try:
        Captcha(request).validate()
        result = 'Captcha valid'
    except CaptchaError as e:
        result = '{0}'.format(e)
    template = Template(template_validate)
    response = Response(template.render(result=result))
    response.status_int = 200
    response.content_type = 'text/html'
    return response


if __name__ == '__main__':
    settings = {
        'pyramid.includes': 'pyramid_beaker',
        'session.type': 'file',
        'session.data_dir': '{0}/.sessions/data'.format(os.getcwd()),
        'session.lock_dir': '{0}/.sessions/lock'.format(os.getcwd()),
        'session.key': 'pyramid_captcha',
        'session.secret': 'pyramid_captcha_secret',
        'session.cookie_on_exception': True
    }
    with Configurator(
            settings=settings,
            session_factory=session_factory_from_settings(settings)
    ) as config:
        config.add_route('index', '/')
        config.add_view(
            index,
            route_name='index',
            request_method='GET'
        )
        config.add_route('validate', '/validate')
        config.add_view(
            validate,
            route_name='validate',
            request_method='POST'
        )
        config.add_route('captcha_generate', '/captcha/generate')
        config.add_view(
            Captcha,
            attr='generate',
            route_name='captcha_generate',
            request_method='GET'
        )
        app = config.make_wsgi_app()
    serve(app, host='0.0.0.0', port=5000)
